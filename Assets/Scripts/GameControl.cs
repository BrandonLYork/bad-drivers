﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class GameControl : MonoBehaviour {
    public static GameControl Instance;
    private float game_speed = 1f;
    private float road_speed = 1f;
    public int game_difficulty = 0;

    int validPosMax = 15;
    int validPosMin = 0;
    public bool game_start = true;

    public bool is_game_over = false;
    public Text score_text;
    public GameObject game_over;
    public GameObject player;
    public GameObject start_screen;
    int score = 0;

    public static float[] valid_Positions = { -7.735f, -6.63f, -5.525f, -4.42f, -3.315f, -2.21f, -1.105f, 0, 1.105f, 2.21f, 3.315f, 4.42f, 5.525f, 6.63f, 7.735f};

    // Use this for initialization
    void Awake () {
        print("Start: " + is_game_over.ToString());
        game_speed = (game_difficulty + 1) * game_speed;
        road_speed = (game_difficulty + 1) * road_speed;

	    if(Instance == null)
        {
            Instance = this;
        } else if(Instance != this)
        {
            Destroy(gameObject);
        }
	}

    void Start()
    {
        if(game_start)
        {
            this.is_game_over = true;
            player.SetActive(true);
            start_screen.SetActive(false);
        }
    }

    // Update is called once per frame
    void Update () {
        //print(Input.GetKeyDown(KeyCode.Space));
        if (is_game_over && Input.GetKeyDown(KeyCode.Space))
        {
            //print("Starting");

            SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
            
        }
    }
    
    public float[] getValidPositions()
    {
        return valid_Positions;
    }

    public void HitCar()
    {
        player.SetActive(false);
        is_game_over = true;
        game_over.SetActive(true);
    }

    public void AddScore(int new_val)
    {
        score += new_val;
        score_text.text = "Score : " + score.ToString();
    }
    
    public float getSpeed()
    {
        return game_speed;
    }

    public int getMaxPos() { return validPosMax; }
    public int getMinPos() { return validPosMin; }
}
