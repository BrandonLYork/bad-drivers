﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ScrollingRoad : MonoBehaviour {

    private Rigidbody2D rb2d;
    public string Type;

	// Use this for initialization
	void Start () {
        rb2d = GetComponent<Rigidbody2D>();
        rb2d.velocity = new Vector2(0, -1 * GameControl.Instance.getSpeed());
	}
	
	// Update is called once per frame
	void Update () {
        
        if(GameControl.Instance.is_game_over)
        {
            rb2d.velocity = Vector2.zero;
        }
        else if(rb2d.velocity == Vector2.zero)
        {
            rb2d.velocity = new Vector2(0, -1 * GameControl.Instance.getSpeed());
        }
	}
}
