﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class RepeatingBackground : MonoBehaviour {

    private BoxCollider2D RoadCollider;
    private float roadLength;

	// Use this for initialization
	void Start () {
        RoadCollider = GetComponent<BoxCollider2D>();
        roadLength = RoadCollider.size.y;
	}
	
	// Update is called once per frame
	void Update () {
		if(transform.position.y < -roadLength)
        {
            RepositionBackground();
        }
        //ScoreText.text = "Test" + leftRoadCollider.size.y;
    }

    private void RepositionBackground()
    {
        Vector2 groundOffset = new Vector2(0, roadLength * 2);
        transform.position = (Vector2)transform.position + groundOffset;
    }
}
