﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Collision : MonoBehaviour {

    public GameObject explosion;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if(GetComponent<PlayerMovement>() != null)
        {
            collision.gameObject.SetActive(false);

            explosion = (GameObject)Instantiate(explosion, new Vector2(0, 0), Quaternion.identity);
            explosion.SetActive(true);
            explosion.transform.position = new Vector2(transform.position.x, transform.position.y + 1);

            Animator anim = explosion.GetComponent<Animator>();

            anim.SetBool("Exploding", true);

            GameControl.Instance.HitCar();
        }
    }
}
