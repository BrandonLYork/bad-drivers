﻿#define DBG
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

enum CAR_TYPES {
    e_BLUE_CAR,
    e_GREEN_CAR,
    e_RED_CAR,
    e_PURPLE_CAR,
    e_CAR_TYPE_SIZE
}



public class CarPool : MonoBehaviour {

    public GameObject blue_car;
    public GameObject green_car;
    public GameObject red_car;
    public GameObject purple_car;

    /* Used to make sure not too many cars are being spawned based on the game difficulty level
        car_counts[lane]*/
    private int[] car_counts;

    /* set the percentage chance each car will spawn based on difficulty */
    private int[] car_rates;
    private int[] debug = { 0, 50, 100, 100 };
    private int[] easy = { 0, 80, 90, 95 };
    private int[] medium = { 0, 60, 80, 90 };
    private int[] hard = { 0, 40, 70, 85 };
    private int[] expert = { 0, 25, 50, 75 };
    private int[][] difficulties;

    // Total lanes being utilized in the game
    private int lanes;
    private int cars_per_lane;
    private int car_amount;
    // Total cars currently spawned in the game
    private int total_cars;


    private List<GameObject> cars;
    private float[] lane_spawn_timer;

    private float default_spawn_y = 6.5f;

    float get_smallest_delay() { return smallest_delay; }
    float get_small_gap() { return smallest_delay * 2; }
    float get_long_delay() { return get_small_gap() * 2; }

	// Use this for initialization
	void Start () {
        
        difficulty_mod = 4 - GameControl.Instance.game_difficulty;
        smallest_delay = difficulty_mod;
        //print(get_long_delay().ToString());

        difficulties = new int[4][];

#if DBG
        print("debug");
        difficulties[0] = expert;
        difficulties[1] = expert;
        difficulties[2] = expert;
        difficulties[3] = expert;
#else
        difficulties[0] = easy;
        difficulties[1] = medium;
        difficulties[2] = hard;
        difficulties[3] = expert;
#endif



        car_rates = difficulties[GameControl.Instance.game_difficulty];

        lanes = GameControl.Instance.getMaxPos() - GameControl.Instance.getMinPos();
        cars_per_lane = (int)CAR_TYPES.e_CAR_TYPE_SIZE;
        car_amount = lanes * cars_per_lane; // 4 max cars per lane
        //cars = new GameObject[car_amount];
        cars = new List<GameObject>();
        lane_spawn_timer = new float[lanes];
        car_counts = new int[lanes];

        
        for (int i = 0;i < lanes; i++)
        {
            lane_spawn_timer[i] = Random.Range(get_smallest_delay(), get_long_delay());// GameControl.Instance.game_speed);
        }
        // 0 - 14 Blue
        // 15 - 29 Green
        // 30 - 44 Red
        // 45 - 59 Purple
        //cars[i + bMod] = (GameObject)Instantiate(blueCar, defaultSpawnPosition, Quaternion.identity);
    }
    // Difficulty mod = the inverse of the difficulty
    // easy = 4, medium = 3, hard = 2, extreme = 1
    float difficulty_mod = 0;
    
    // Minimum spawn timer is 2 seconds 
    float smallest_delay = 2f;
    // Minimum spawn timer to allow player movement is 4
    //  Player can move maximum of 4 cars with a delay of 4 seconds

    // Update is called once per frame
    void Update () {
        float delta = Time.deltaTime;// = Time since this was last run

        for (int i = 0; i < cars.Count; i++)
        {
            if (cars[i].GetComponent<EnemyCar>().killMe())
            {
                GameObject temp = cars[i];
                car_counts[temp.GetComponent<EnemyCar>().getLane() - GameControl.Instance.getMinPos()] -= 1;
                cars.Remove(temp);
                Destroy(temp);
                i--;
            }
        }

        if (!GameControl.Instance.is_game_over)
        {
            for (int i = 0; i < lanes; i++)
            {
                lane_spawn_timer[i] -= delta;
                if (lane_spawn_timer[i] <= 0)
                {
                    // reset timer
                    lane_spawn_timer[i] = Random.Range(get_small_gap(), get_long_delay()); // 10);

                    // Add a car if there aren't too many
                    int to_spawn = Random.Range(0, 100);
                    //print(to_spawn.ToString());
                    GameObject new_car = null;
                    //print(to_spawn.ToString() + car_rates[2].ToString());
                    if (to_spawn < car_rates[1])
                    {
                        new_car = blue_car;
                    }
                    else if (to_spawn < car_rates[2])
                    {
                        // Green
                        new_car = green_car;
                    }
                    else if (to_spawn < car_rates[3])
                    {
                        // Red
                        new_car = red_car;
                    }
                    else
                    {
                        // purple
                        new_car = purple_car;
                    }

                    if (car_counts[i] < 4 && new_car != null)
                    {
                        car_counts[i] += 1;
                        GameObject car = Instantiate(new_car, new Vector2(GameControl.Instance.getValidPositions()[i+GameControl.Instance.getMinPos()], default_spawn_y), Quaternion.identity);
                        car.GetComponent<BoxCollider2D>().enabled = false;
                        car.GetComponent<BoxCollider2D>().enabled = true;
                        car.GetComponent<EnemyCar>().setLane(GameControl.Instance.getMinPos() + i);
                        cars.Add(car);
                    }
                }
            }
        }
	}
}
