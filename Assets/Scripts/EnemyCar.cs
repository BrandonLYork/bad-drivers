﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyCar : MonoBehaviour {
    private Rigidbody2D rb2d;
    //public string Type;

    public int Type;
    private CAR_TYPES myType;
    private int ID;
    private BoxCollider2D car_Collider;
    private float car_Length;
    private float car_Offset;

    private bool kill_me = false;
    private int my_lane = -1;

    private float special_mod = 0;
    private float special_timer = 0;
    private int special_flag = 0;

    private float speed = 0;

    void setSpeed(Rigidbody2D rb2d)
    {
        rb2d.velocity = new Vector2(0, -1 * GameControl.Instance.getSpeed() + speed);
    }

    void setSpeed(Rigidbody2D rb2d, CAR_TYPES _Type)
    {
        rb2d.velocity = Vector2.zero;
        switch (_Type)
        {
            case CAR_TYPES.e_BLUE_CAR:
                //myType = CAR_TYPES.e_BLUE_CAR;
                rb2d.velocity = new Vector2(0, -1 * GameControl.Instance.getSpeed());
                break;
            case CAR_TYPES.e_GREEN_CAR:
                //myType = CAR_TYPES.e_GREEN_CAR;
                special_mod = .5f;
                speed = -2;
                break;
            case CAR_TYPES.e_RED_CAR:
                //myType = CAR_TYPES.e_RED_CAR;
                speed = -4;
                break;
            case CAR_TYPES.e_PURPLE_CAR:
                //myType = CAR_TYPES.e_PURPLE_CAR;
                speed = -6;
                break;
        }
        setSpeed(rb2d);
    }

    // Use this for initialization
    void Start () {
        //print(my_lane.ToString());
        myType = (CAR_TYPES)Type;

        rb2d = GetComponent<Rigidbody2D>();

        setSpeed(rb2d,myType);


        special_timer = Random.Range(0, 1f);

        car_Offset = GameControl.Instance.getSpeed();
        if (car_Offset > 10) car_Offset = 10;
        else if (car_Offset <= 0) car_Offset = 1;
        car_Collider = GetComponent<BoxCollider2D>();
        car_Length = car_Collider.size.y;
    }

    // Update is called once per frame
    void Update()
    {
        
        if (GameControl.Instance.is_game_over)
        {
            rb2d.velocity = Vector2.zero;
        }
        if (transform.position.y < -(car_Length))
        {
            Reposition();
        }

        float delta = Time.deltaTime;// = Time since this was last run
        if (!GameControl.Instance.is_game_over)
        {
            if (special_flag == 1 || special_flag == -1)
            {
                transform.Translate(new Vector3(special_flag*.05f, 0));
                if (special_flag == -1) // left
                {
                    //print(my_lane.ToString());
                    if(transform.position.x <= GameControl.Instance.getValidPositions()[my_lane-1])
                    {
                        my_lane--;
                        transform.position = new Vector2(GameControl.Instance.getValidPositions()[my_lane], getY());
                        special_flag = 0;
                        setSpeed(rb2d, myType);
                    }
                }
                else if(special_flag == 1) // right
                {
                    if (transform.position.x >= GameControl.Instance.getValidPositions()[my_lane+1])
                    {
                        my_lane++;
                        transform.position = new Vector2(GameControl.Instance.getValidPositions()[my_lane], getY());
                        special_flag = 0;
                        setSpeed(rb2d, myType);
                    }
                }
            }

            special_timer = special_timer - delta;
            if(special_timer <= 0)
            {
                //print("Special Move");
                special_timer = Random.Range(0.2f, 0.5f+special_mod);
                special_move();
            }
        }
    }

    private void Reposition()
    {
        //Vector2 totalOffset = new Vector2(0, (car_Length * 2) + (10 - car_Offset));
        //transform.position = (Vector2)transform.position + totalOffset;
        rb2d.velocity = Vector2.zero;
        kill_me = true;
        if(!GameControl.Instance.is_game_over)
            GameControl.Instance.AddScore((int)myType + 1);
    }


    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.GetComponent<EnemyCar>() != null)
        {
            if (getLane() != collision.GetComponent<EnemyCar>().getLane())
            {
                transform.position = new Vector2(GameControl.Instance.getValidPositions()[getLane()], getY());
                special_flag = 0;
                return;
            }

            if (speed < collision.GetComponent<EnemyCar>().getSpeed())
            {
                //    //print("Slowing down" + myType.ToString() + " " + speed.ToString());
                //    //Type = (int)speed;
                //if (special_flag == -2) { print("Red Car WHY"); }
                
                speed = collision.GetComponent<EnemyCar>().getSpeed();
                print("Collide type: " + myType.ToString() + " speed: " + speed.ToString() + " collision speed: " + collision.GetComponent<EnemyCar>().getSpeed().ToString());
                setSpeed(rb2d);
                transform.position = new Vector2(transform.position.x, transform.position.y + .2f);

            }

        }
    }

    public int getID() { return ID; }
    public bool killMe() { return kill_me; }

    public void setLane(int new_lane) { my_lane = new_lane; }
    public int getLane() { return my_lane; }
    private CAR_TYPES getMyType() { return myType; }

    public float getY() { return transform.position.y; }

    private void special_move()
    {
        switch (myType)
        {
            case CAR_TYPES.e_BLUE_CAR: break;
            case CAR_TYPES.e_GREEN_CAR:
                speed = -6;
                setSpeed(rb2d);
                special_flag = -2;
                
                break;
            case CAR_TYPES.e_RED_CAR:
                if (special_flag == 0)
                {
                    int dir = Random.Range((int)0, (int)1);
                    //print(my_lane.ToString());
                    if (dir == 0 && my_lane > GameControl.Instance.getMinPos())
                    {

                        //transform.position = new Vector2(, getY());
                        special_flag = -1;

                    }
                    else if (my_lane < GameControl.Instance.getMaxPos())
                    {
                        special_flag = 1;

                        //transform.position = new Vector2(GameControl.Instance.getValidPositions()[my_lane], getY());
                    }
                }
                break;
            case CAR_TYPES.e_PURPLE_CAR:
                if (special_flag == 2)
                {
                    speed = -6;
                }
                else
                {
                    speed = 2;
                    special_flag = 2;
                }
                setSpeed(rb2d);
                break;
        }
    }

    float getSpeed() { return speed; }
}
