﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMovement : MonoBehaviour {

    float yPos = -2.5f;
    //int validPosMax = 15;
    int validPosMax = 7;
    int validPosMin = 7;
    int currentPos = 7;
    
    public bool isPlayerCar = false;
    Vector2 myPos;
	// Use this for initialization
	void Start () {
        validPosMax = GameControl.Instance.getMaxPos();
        validPosMin = GameControl.Instance.getMinPos();
        transform.position = new Vector2(0, yPos);
	}
	
	// Update is called once per frame
	void Update () {
        bool updatePos = false;
        if (isPlayerCar && !GameControl.Instance.is_game_over)
        {
            if (Input.GetKeyDown(KeyCode.LeftArrow))
            {
                currentPos--;
                if (currentPos < validPosMin) currentPos = validPosMin;
                updatePos = true;
            }
            else if (Input.GetKeyDown(KeyCode.RightArrow))
            {
                currentPos++;
                if (currentPos >= validPosMax) currentPos = validPosMax - 1;
                updatePos = true;
            }
            //print(currentPos.ToString());
            if (updatePos) transform.position = new Vector2(GameControl.Instance.getValidPositions()[currentPos], yPos);
        }
	}
}
